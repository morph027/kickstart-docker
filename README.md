# Kickstart

This will install a Docker host.

The Kickstart recipe is basically a minimal CentOS, with all the magic happening in ```%post``` section.

As always, please consider this as a template or proof of concept. You know best of all which things you want to get kickstarted ;)

You might be interested in [dynamic Kickstart files](https://gitlab.com/snippets/26997) too. I'm also using it in conjunction with the [Proxmox ansible module](https://docs.ansible.com/ansible/proxmox_kvm_module.html).

## PXE

Add to any PXE boot menu file:

```
LABEL yes-centos7
	MENU LABEL CentOS Docker - DISK WILL BE WIPED (7)
	KERNEL centos/7/vmlinuz
	APPEND initrd=centos/7/initrd.img inst.stage2=http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/ inst.repo=http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/ inst.ks=https://gitlab.com/morph027/kickstart-docker/raw/master/docker.cfg rd.noverifyssl --
	SYSAPPEND 2
```

Also add installer files to PXE server, if not already there (pxe folder might be different):

```
export PXE_FOLDER="/var/lib/tftpboot"
mkdir -p $PXE_FOLDER/centos/7
cd $PXE_FOLDER/centos/7
wget http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/isolinux/vmlinuz
wget http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/isolinux/initrd.img
```

## Credentials

When installation is finished, the root password is ```centos```. 

## Hostname

* You can provide the desired hostname while booting by appending ```HOSTNAME=foobar``` to the ```APPEND``` line (by hitting Tab).
* Or you just can set it afterwards: ```hostnamectl set-hostname gitlab-ci-docker-runner-X```
* Might use [Dynamic Kickstart](#dynamic-kickstart):

## Network

You can submit the following values in kernel cmdline to set up the network:

* ```IP=xxx.xxx.xxx.xxx/xx``` (required for setup, nothing happens if this is missing and other options are present)
* ```GW=xxx.xxx.xxx.xxx``` (additional)
* ```DNS=xxx.xxx.xxx.xxx``` (additional)

## Examples

### complete network config

```
...
	APPEND initrd=$all-the-stuff-from-above HOSTNAME=foobar IP=10.10.10.42/24 GW=10.10.10.254 DNS=10.10.10.253 --
...
```
